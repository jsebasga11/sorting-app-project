import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class SortingTest {
    Sorting sorting = new Sorting();

    @Test(expected = IllegalArgumentException.class)
    public void testNullCase(){
        sorting.sort(null);
    }

    @Test
    public void testEmptyCase(){
        List<Integer> emptyArray = new ArrayList<>();
        sorting.sort(emptyArray);
        Assert.assertTrue(emptyArray.isEmpty());
    }

    @Test
    public void testSingleElementArrayCase() {
        List<Integer> singleElementArray = Arrays.asList(1);
        sorting.sort(singleElementArray);
        Assert.assertEquals(singleElementArray, Arrays.asList(1));
    }

    @Test
    public void testSortedArraysCase() {
        List<Integer> sortedArray = Arrays.asList(1,3,5,7);
        sorting.sort(sortedArray);
        Assert.assertEquals(sortedArray, Arrays.asList(1,3,5,7));
    }

    @Test
    public void testOtherCases() {
        List<Integer> repeatedArray = Arrays.asList(3,3,3,3,5,3);
        sorting.sort(repeatedArray);
        Assert.assertEquals(repeatedArray, Arrays.asList(3,3,3,3,3,5));
    }

}