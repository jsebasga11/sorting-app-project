import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class CornerCasesTest {
    Numbers numbers = new Numbers();

    int integersNum;

    public CornerCasesTest(int integersNum) {
        this.integersNum = integersNum;
    }

    @Parameterized.Parameters
    public static Collection<Object> data(){
        return Arrays.asList(new Object[][]{
                {0},
                {11},
                {-4}
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void requestNumbers() {
        numbers.requestNumbers(integersNum);
    }
}