import java.util.*;

public class Sorting {
    public void sort(List<Integer> array) {
        if (Objects.isNull(array)) {
            throw new IllegalArgumentException();
        }

        Collections.sort(array);
    }
}
