import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Numbers {
    List<Integer> numbersList;

    public Numbers() {
        this.numbersList = new ArrayList<>();
    }

    public void requestNumbers(Integer integersNum){
        Scanner scanner = new Scanner(System.in);
        if( integersNum <= 0 || integersNum > 10){
            throw new IllegalArgumentException("El valor debe estar entre 1 y 10");
        }
        for (int i=1; i<=integersNum; i++ ){
            System.out.println(String.format("Introduce el valor número %d", i));
            this.numbersList.add(scanner.nextInt());
        }
    }
}
