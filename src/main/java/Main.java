import java.util.*;

public class Main {
    public static void main(String[] args) {
        Sorting sorting = new Sorting();
        Numbers numbers = new Numbers();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Introduce la cantidad de números que vas a ingresar, el valor máximo es 10");
        int integersNum = scanner.nextInt();

        numbers.requestNumbers(integersNum);
        sorting.sort(numbers.numbersList);
        System.out.println(numbers.numbersList);
    }

}
